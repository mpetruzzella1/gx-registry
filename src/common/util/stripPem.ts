export class Util {
  static stripPEMInfo(PEMInfo: string): string {
    return PEMInfo.replace(/\r\n|\r|\n/g, '').replace(/([']*-----(BEGIN|END) (CERTIFICATE|PKCS7)-----[']*|\n)/gm, '')
  }
}
