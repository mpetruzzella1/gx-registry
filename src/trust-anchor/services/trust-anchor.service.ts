import { ValidationResult } from 'joi'
import { isEmpty } from '../../common/util'
import { BadRequestException, ConflictException, Injectable, Logger, NotFoundException } from '@nestjs/common'
import { TrustStates } from '../../common/interfaces'
import { CreateTrustAnchorDto, TrustAnchorRequestDto, TrustAnchorResponseDto } from '../dto'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { TrustAnchor, TrustAnchorDocument } from '../schemas'
import { DEFAULT_UPSERT_OPTIONS } from '../constants'
import * as crypto from 'crypto'
import Graph from 'graphology'
import { Util } from '../../common/util/stripPem'

@Injectable()
export class TrustAnchorService {
  private readonly logger = new Logger(TrustAnchorService.name)

  public static readonly CRYPTO_ENGINE_NAME = 'Crypto Engine'
  public static readonly TRUST_ANCHOR_CERTIFICATES_CACHE_KEY = 'TrustAnchorCertificates'

  constructor(@InjectModel(TrustAnchor.name) public trustAnchorModel: Model<TrustAnchorDocument>) {}

  /**
   * Validates a chain of X509 certificates
   * @param base64Certs the raw base64 encoded certificate strings
   * @returns {boolean} the verification result
   */
  public async validateCertChain(base64Certs: string[]): Promise<boolean> {
    // Verify cert chain is not broken
    await this.assertCertChainContinous(base64Certs)
    // Create Certificate instances from raw strings
    let validationresult = false
    try {
      await Promise.all(
        base64Certs.map(async element => {
          const certificat: TrustAnchorRequestDto = { certificate: element }
          if (await this.trustAnchorModel.findOne(certificat).exec()) {
            validationresult = true
          }
        })
      )
    } catch (error) {
      this.logger.error(error)
      throw new ConflictException('Certificate Chain could not be validated.', error.toString())
    }
    if (!validationresult) {
      throw new NotFoundException('Chain certificate not found ')
    }
    return validationresult
  }

  /**
   * Retrieve all trustAnchors from the database
   * @returns {Promise<TrustAnchor[]>} the trustAnchors found in the DB
   */
  public async getAllTrustAnchors(): Promise<TrustAnchor[]> {
    return await this.trustAnchorModel.find().exec()
  }

  /**
   * Searches for a trustAnchor in the database given a certain certificate and returns a trustAnchorResponse if one was found.
   * Will throw a 404 exception in case no trustAnchor could be retrieved from the database.
   * @param trustAnchorData the body containing a certificate
   * @returns {Promise<TrustAnchorResponseDto>} the prepared trustAnchorResponse
   */
  public async findTrustAnchor(trustAnchorData: ValidationResult<TrustAnchorRequestDto>['value']): Promise<TrustAnchorResponseDto> {
    if (isEmpty(trustAnchorData)) throw new BadRequestException('Request body invalid.')
    const { certificate } = trustAnchorData
    const findTrustAnchor = await this.trustAnchorModel.findOne({ certificate }).exec()

    if (!findTrustAnchor) throw new NotFoundException('Trust Anchor not found.')

    return await this.prepareTrustAnchorResponse(findTrustAnchor)
  }

  /**
   * Prepares a trustAnchorResponse given a certain trustAnchor
   * @param trustAnchor the trustAnchor to prepare the response object for
   * @returns {Promise<TrustAnchorResponseDto>} the prepared trustAnchorResponse
   */
  private async prepareTrustAnchorResponse(trustAnchor: TrustAnchorDocument): Promise<TrustAnchorResponseDto> {
    return {
      trustState: TrustStates.Trusted,
      trustedForAttributes: new RegExp('.*', 'gm').toString(),
      trustedAt: trustAnchor.lastTimeOfTrust?.getTime()
    }
  }

  /**
   * Update TrustAnchors in the DB from a given {@link CreateTrustAnchorDto} array
   * @param {CreateTrustAnchorDto[]} trustAnchors the trust anchors to update
   * @param {Object} options an options object
   * @param {QueryOptions} options query options to be passed to the database update call
   * @returns {Promise<number>} a promise resolving to the number of db entries that were updated
   */
  async updateTrustAnchors(trustAnchors: CreateTrustAnchorDto[], options = DEFAULT_UPSERT_OPTIONS) {
    const mongoPromises = []
    for (const ta of trustAnchors) {
      // find trustAnchors by certificate & _list id
      const { certificate, _list } = ta
      const updatePromise = this.trustAnchorModel
        .findOneAndUpdate({ certificate, _list }, ta, options)
        .exec()
        .catch(e => this.logger.error(e))
      mongoPromises.push(updatePromise)
    }

    await Promise.all(mongoPromises)

    return mongoPromises.length
  }

  private async assertCertChainContinous(base64Certs: string[]) {
    const graph = new Graph()
    let isolatedNodes = []
    for (const cert of base64Certs.reverse()) {
      const certObj = new crypto.X509Certificate('-----BEGIN CERTIFICATE-----\n' + cert + '\n-----END CERTIFICATE-----')
      if (!graph.hasNode(certObj.subject) && (certObj.issuer !== certObj.subject || (await this.isCAOrTrustedAnchor(certObj)))) {
        graph.addNode(certObj.subject, { ca: certObj.ca })
      }
      const parentNode = graph.nodes().find(node => node === certObj.issuer)
      // Only allowed cert to point towards themselves are CA
      if (!!parentNode) {
        graph.addEdge(certObj.subject, certObj.issuer)
      } else {
        isolatedNodes.push({ subject: certObj.subject, issuer: certObj.issuer })
      }
    }
    //After parsing all certs, we try to connect isolated nodes to each others.
    isolatedNodes = isolatedNodes
      .map(node => {
        if (graph.hasNode(node.issuer)) {
          graph.addEdge(node.subject, node.issuer)
          return null
        } else {
          return node
        }
      })
      .filter(n => !!n)
    if (isolatedNodes.length > 0) {
      throw new ConflictException(`Certificate(s) ${isolatedNodes.map(node => node.subject).join(',')} in the chain don't not belong to the chain`)
    }
  }

  private async isCAOrTrustedAnchor(certObj: crypto.X509Certificate) {
    // try to find cert in anchors
    const certWithoutLineFeedAndHeaders = Util.stripPEMInfo(certObj.toString()).replace(/\n/, '')
    const certWithoutHeaders = Util.stripPEMInfo(certObj.toString())
    const trustedAnchors = await this.trustAnchorModel.findOne({
      $or: [{ certificate: certObj.toString() }, { certificate: certWithoutLineFeedAndHeaders }, { certificate: certWithoutHeaders }]
    })
    return trustedAnchors !== null
  }
}
