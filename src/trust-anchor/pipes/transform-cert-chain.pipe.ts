import { BadRequestException, Injectable, Logger, PipeTransform } from '@nestjs/common'
import { CertificateChainDto, TrustAnchorChainRequestDto } from '../dto'
import { Util } from '../../common/util/stripPem'

@Injectable()
export class CertChainTransformPipe implements PipeTransform<TrustAnchorChainRequestDto, Promise<CertificateChainDto>> {
  private readonly logger = new Logger(CertChainTransformPipe.name)

  async transform(body: TrustAnchorChainRequestDto): Promise<CertificateChainDto> {
    try {
      const { certs } = body
      // split string into 1 item per certificate
      const split = certs.split('-----BEGIN CERTIFICATE-----')

      // remove BEGIN CERTIFICATE etc. and filter empty leftover strings
      const raw = split.map(c => Util.stripPEMInfo(c)).filter(c => c.length > 1)

      return {
        certs: raw
      }
    } catch (error) {
      this.logger.error(error)
      throw new BadRequestException('Certificate chain could not be transformed.')
    }
  }
}
