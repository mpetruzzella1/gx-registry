import { Controller, Get, Param, Response } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { ShapeService2210 } from './services'
import { ShapeFilesApiResponse } from './decorators'
import { FileTypes } from './constants'
import { Response as Res } from 'express'

@ApiTags('Trusted-Shape-registry')
@Controller({ path: '/api/trusted-shape-registry/v1/shapes' })
export class TrustedShapeRegistry {
  constructor(private readonly shapesService: ShapeService2210) {}

  @ApiOperation({ summary: 'Get a JSONLD context for all available shapes (2210).' })
  @ApiOkResponse({ description: 'The JSONLD context for all available shapes in the registry.' })
  @Get()
  async getVersion(): Promise<any> {
    return this.shapesService.getJsonldContext()
  }

  @ApiOperation({ summary: 'Get a JSONLD shape' })
  @ApiOkResponse({ description: 'The JSONLD shape.' })
  @Get('/jsonld/:key')
  async getJSONLDShape(@Param('key') key: string, @Response({ passthrough: true }) res: Res): Promise<any> {
    res.set({ 'Content-Type': `application/ld+json` })
    return this.shapesService.getJsonLdShape(key)
  }

  @ShapeFilesApiResponse('Get specified SHACL file as ttl according to its identifier')
  @Get('/:key')
  getShapeVersioned(@Param() params, @Response({ passthrough: true }) res: Res): string {
    try {
      const shape = this.shapesService.getFileByType(params.key, FileTypes.ttl)
      res.set({ 'Content-Type': `text/turtle` })
      return shape
    } catch (e) {
      throw e
    }
  }
}
