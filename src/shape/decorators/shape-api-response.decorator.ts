import { applyDecorators } from '@nestjs/common'
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger'

export function ShapeFilesApiResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiParam({ name: 'key', required: true, description: 'ID of the shapes you wants to query' }),
    ApiBadRequestResponse({ description: 'Shape not found' }),
    ApiOkResponse({ description: 'SHACL file asked as ttl' }) // TODO add me
  )
}
