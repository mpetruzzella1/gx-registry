import { Module } from '@nestjs/common'
import { ShapeService2210 } from './services'
import { TrustedShapeRegistry } from './trusted-shape-registry.controller'

@Module({
  imports: [],
  controllers: [TrustedShapeRegistry],
  providers: [ShapeService2210]
})
export class TrustedShapeRegistry2210Module {}
