import { Injectable, Logger, NotFoundException } from '@nestjs/common'
import { join } from 'path'
import { Readable } from 'stream'
import fs from 'fs'
import * as ttl2jsonld from '@frogcat/ttl2jsonld'
import { FileTypes } from '../constants'

@Injectable()
export class ShapeService2210 {
  private readonly logger = new Logger('Shape')

  getFileByType(file: string, type: FileTypes): string {
    try {
      if (FileTypes.jsonld === type) {
        return this.getJsonLdShape(file)
      }
      return this.getTTLFile(file)
    } catch (e) {
      throw new NotFoundException('Desired shaped not found, shape incorrect or unimplemented')
    }
  }

  getJsonLdShape(key: string): any {
    const ttlFile = this.getTTLFile(key)
    return this.convertToJsonld(ttlFile)
  }

  async getJsonldContext(): Promise<any> {
    try {
      const context = {
        '@version': 2210
      }

      const shapeFiles = await this.getAvailableShapeFiles()
      for (const file of shapeFiles) {
        const ttlFile = this.getTTLFile(file)
        context[`gx-${file}`] = this.convertToJsonld(ttlFile)
      }

      return context
    } catch (e) {
      console.log('error', e.message)
      throw e
    }
  }

  async getAvailableShapeFiles(): Promise<Array<string>> {
    try {
      return fs.readdirSync(join(__dirname, `../../static/shapes`)).map(filename => filename.split('.')[0])
    } catch (e) {
      throw new NotFoundException('Desired shaped not found, version is incorrect or unimplemented')
    }
  }

  private getTTLFile(filename): string {
    try {
      fs.accessSync(join(__dirname, `../../static/shapes/${filename}.ttl`))
      const ttl = fs.readFileSync(join(__dirname, `../../static/shapes/${filename}.ttl`))
      return ttl.toString().replace(/\$BASE_URL/g, process.env.BASE_URL)
    } catch (Error) {
      throw new NotFoundException('Desired shape not found, shape name is incorrect or unimplemented')
    }
  }

  private convertToJsonld(ttl: string): any {
    return ttl2jsonld.parse(ttl)
  }

  private getReadable(input: string) {
    return Readable.from(input)
  }
}
