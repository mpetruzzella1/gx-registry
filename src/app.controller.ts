import { Controller, Get } from '@nestjs/common'
import { ApiExcludeController } from '@nestjs/swagger'
import { bugs, description, name, repository, version } from '../package.json'
import { SWAGGER_UI_PATH } from './common/util'

@ApiExcludeController()
@Controller()
export class AppController {
  @Get()
  getDescription() {
    return {
      software: name,
      description,
      version,
      documentation: `${process.env.BASE_URL}${SWAGGER_UI_PATH}/`,
      repository,
      bugs
    }
  }
}
