import { applyDecorators } from '@nestjs/common'
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation, ApiParam } from '@nestjs/swagger'

export function SchemaFilesApiResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiParam({ name: 'key', required: true, description: 'ID of the schemas you wants to query' }),
    ApiBadRequestResponse({ description: 'Shape not found' }),
    ApiOkResponse({ description: 'Schemas as Jsonld' }) // TODO add me
  )
}
