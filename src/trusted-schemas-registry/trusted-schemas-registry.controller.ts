import { Controller, Get, Param, StreamableFile } from '@nestjs/common'
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger'
import { SchemasService2210 } from './service'
import { SchemaFilesApiResponse } from './decorator'
import { FileTypes } from './constant'

@ApiTags('Trusted-Schema-registry')
@Controller({ path: '/api/trusted-schemas-registry/v2/schemas' })
export class TrustedSchemasRegistry {
  constructor(private readonly schemasService: SchemasService2210) {}

  @ApiOperation({ summary: 'Get a JSONLD context for all available schemas (2210).' })
  @ApiOkResponse({ description: 'The JSONLD context for all available schemas in the registry.' })
  @Get()
  async getVersion(): Promise<any> {
    return this.schemasService.getJsonldContext()
  }

  @SchemaFilesApiResponse('Get specified schemas in jsonld format according to its identifier')
  @Get('/:key')
  async getShapeVersioned(@Param() params): Promise<StreamableFile | Array<string>> {
    try {
      return await this.schemasService.getFileByType(params.key, FileTypes.jsonld)
    } catch (e) {
      console.log('Unable to get schema', e)
      throw e
    }
  }
}
