import { applyDecorators } from '@nestjs/common'
import { ApiBadRequestResponse, ApiOkResponse, ApiOperation } from '@nestjs/swagger'

export function TermsAndConditionsApiResponse(summary: string) {
  return applyDecorators(
    ApiOperation({ summary }),
    ApiBadRequestResponse({ description: 'Invalid request query' }),
    ApiOkResponse({ description: 'The version, terms and conditions text and the sha256 of the text.' })
  )
}
