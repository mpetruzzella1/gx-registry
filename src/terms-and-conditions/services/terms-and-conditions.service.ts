import { Injectable } from '@nestjs/common'
import { termsAndConditions } from '../constants'

@Injectable()
export class TermsAndConditionsService {
  getAvailableTermsAndConditions(): any {
    return termsAndConditions
  }
}
