import { Controller, Get, UsePipes } from '@nestjs/common'
import { ApiTags } from '@nestjs/swagger'
import { TermsAndConditionsService } from './services'
import { JoiValidationPipe } from '../common/pipes'
import { versionSchema } from './schemas/version.schema'
import { TermsAndConditionsApiResponse } from './decorators'

@ApiTags('TermsAndConditions')
@Controller({ path: '/api/termsAndConditions' })
export class TermsAndConditionsController {
  constructor(private readonly termsAndConditionsService: TermsAndConditionsService) {}

  @TermsAndConditionsApiResponse('Get terms and conditions for specified version or a list of available versions when no version is specified.')
  @UsePipes(new JoiValidationPipe(versionSchema))
  @Get()
  async getTermsAndConditions() {
    return this.termsAndConditionsService.getAvailableTermsAndConditions()
  }
}
